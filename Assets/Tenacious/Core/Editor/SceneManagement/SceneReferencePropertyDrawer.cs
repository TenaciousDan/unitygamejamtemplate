using UnityEditor;

using UnityEngine;

using TenaciousEditor.Utilities;

using Tenacious.SceneManagement;

namespace TenaciousEditor
{
    [CustomPropertyDrawer(typeof(SceneReference))]
    public class SceneReferencePropertyDrawer : PropertyDrawer
    {
        private const string SCENE_ASSET_FIELD_NAME = "sceneAsset";
        private const string SCENE_PATH_FIELD_NAME = "scenePath";

        private float addRemoveButtonDrawWidth = EditorGUIUtil.CalcTextSize("000").x;
        private float buildIndexFieldDrawWidth = EditorGUIUtil.CalcTextSize("0000").x;

        protected Color originalBackgroundColor;
        protected float originalLabelWidth;
        protected float height = -1;
        protected Vector2 startPosition;
        protected float horizontalSpacing = 2f;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (height == -1) OnGUI(new Rect(), property, label);

            return height;
        }

        private void ResetDrawerProperties(Rect position)
        {
            height = 0;
            originalBackgroundColor = GUI.backgroundColor;
            originalLabelWidth = EditorGUIUtility.labelWidth;
        }

        private Rect MoveRectHorizontal(ref Rect position, float drawWidth = -1)
        {
            drawWidth = drawWidth < 0 ? position.width : drawWidth;

            Rect rect = new Rect(position.x, position.y, drawWidth, EditorGUIUtility.singleLineHeight);

            AdjustPositionHorizontal(ref position, ref rect);

            return rect;
        }

        private void AdjustPositionHorizontal(ref Rect position, ref Rect rect)
        {
            float horizontalSpaceUsed = rect.width + horizontalSpacing;
            position.x += horizontalSpaceUsed;
            position.width -= horizontalSpaceUsed;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ResetDrawerProperties(position);

            SerializedProperty spSceneAsset = property.FindPropertyRelative(SCENE_ASSET_FIELD_NAME);
            EditorBuildSceneInfo editorBuildSceneInfo = EditorBuildUtil.GetBuildScene(spSceneAsset.objectReferenceValue);

            Rect startPositionRect = position;

            EditorGUI.BeginProperty(position, GUIContent.none, property);
            {
                EditorGUI.BeginChangeCheck();
                {
                    if (editorBuildSceneInfo.buildIndex < 0)
                        GUI.backgroundColor = Color.red;

                    spSceneAsset.objectReferenceValue = EditorGUI.ObjectField(
                        MoveRectHorizontal(ref position, position.width - addRemoveButtonDrawWidth - buildIndexFieldDrawWidth),
                        label.text,
                        spSceneAsset.objectReferenceValue,
                        typeof(SceneAsset),
                        false
                    );

                    GUI.backgroundColor = originalBackgroundColor;
                }
                if (EditorGUI.EndChangeCheck())
                {
                    string sceneAssetPath = null;
                    if (spSceneAsset.objectReferenceValue as SceneAsset != null)
                        sceneAssetPath = AssetDatabase.GetAssetPath(spSceneAsset.objectReferenceValue);

                    if (string.IsNullOrEmpty(sceneAssetPath)) property.FindPropertyRelative(SCENE_PATH_FIELD_NAME).stringValue = string.Empty;
                }

                Rect addRemoveButtonDrawRect = MoveRectHorizontal(ref position, addRemoveButtonDrawWidth);
                if (editorBuildSceneInfo.buildIndex < 0)
                {
                    if (GUI.Button(addRemoveButtonDrawRect, new GUIContent("+", "Add to Build")))
                    {
                        if (!string.IsNullOrEmpty(editorBuildSceneInfo.assetPath))
                            EditorBuildUtil.AddSceneToBuild(editorBuildSceneInfo.assetPath);
                    }
                }
                else
                {
                    GUI.backgroundColor = Color.red;
                    if (GUI.Button(addRemoveButtonDrawRect, new GUIContent("x", "Remove from Build")))
                        EditorBuildUtil.RemoveSceneFromBuild(ref editorBuildSceneInfo);
                }

                GUI.backgroundColor = originalBackgroundColor;

                DrawBuildSceneIndex(ref position, ref editorBuildSceneInfo);
            }
            EditorGUI.EndProperty();

            position = startPositionRect;
            position.y += EditorGUIUtility.singleLineHeight;
            height += EditorGUIUtility.singleLineHeight;
        }

        private void DrawBuildSceneIndex(ref Rect position, ref EditorBuildSceneInfo editorBuildScene)
        {
            EditorGUIUtility.labelWidth = 0.0001f;

            if (editorBuildScene.buildIndex < 0)
                GUI.backgroundColor = Color.red;

            GUIContent buildIndexlabel = new GUIContent(
                " ",
                "The index of the scene asset in the build settings dialog. " +
                "You can modify this value to shift the scene around in the build settings. " +
                "\nRED or -1 means it's NOT included in build. " +
                "\nA positive number means it's included in build."
            );

            Rect buildIndexFieldDrawRect = MoveRectHorizontal(ref position);
            int buildIndex = EditorGUI.DelayedIntField(buildIndexFieldDrawRect, buildIndexlabel, editorBuildScene.buildIndex);

            int numBuildScenes = EditorBuildSettings.scenes.Length;
            if (buildIndex > numBuildScenes)
                buildIndex = numBuildScenes;
            else
                buildIndex = Mathf.Clamp(buildIndex, -1, numBuildScenes - 1);

            if (!string.IsNullOrEmpty(editorBuildScene.assetPath))
            {
                if (editorBuildScene.buildIndex != buildIndex)
                {
                    if (editorBuildScene.buildIndex < 0)
                        EditorBuildUtil.AddSceneToBuild(editorBuildScene.assetPath, buildIndex);
                    else
                        EditorBuildUtil.SetBuildIndex(ref editorBuildScene, buildIndex);
                }
            }

            EditorGUIUtility.labelWidth = originalLabelWidth;
            GUI.backgroundColor = originalBackgroundColor;

            EditorGUI.LabelField(buildIndexFieldDrawRect, buildIndexlabel);
        }
    }
}
