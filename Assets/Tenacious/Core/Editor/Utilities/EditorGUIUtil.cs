﻿using UnityEditor;

using UnityEngine;

namespace TenaciousEditor.Utilities
{
    public static class EditorGUIUtil
    {
        public static Vector2 CalcTextSize(string text)
        {
            return CalcTextSize(text, GUI.skin.label);
        }
        public static Vector2 CalcTextSize(string text, GUIStyle style)
        {
            return style.CalcSize(new GUIContent(text));
        }
    }
}
