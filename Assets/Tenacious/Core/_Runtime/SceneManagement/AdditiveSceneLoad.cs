using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;

using Tenacious.SceneManagement;

namespace Tenacious
{
    /// <summary>
    /// A script used to load a list of other scenes additively.
    /// </summary>
    public class AdditiveSceneLoad : MonoBehaviour
    {
        [Tooltip("A list of scenes to load additively")]
        [SerializeField] private List<SceneInfo> scenes = new List<SceneInfo>();

        [Serializable]
        public class SceneInfo
        {
            public SceneReference sceneRef;
            public bool async;
        }

        private int numScenesToLoad;
        private int numScenesLoaded;

        private void Awake()
        {
            numScenesToLoad = scenes.Count;

            for (int i = 0; i < scenes.Count; ++i)
            {
                SceneInfo sceneInfo = scenes[i];
                if (sceneInfo == null)
                {
                    --numScenesToLoad;
                    continue;
                }

                if (!sceneInfo.async)
                {
                    SceneManager.LoadScene(sceneInfo.sceneRef, LoadSceneMode.Additive);
                    ++numScenesLoaded;
                }
                else
                    StartCoroutine(CRLoadSceneAsync(sceneInfo.sceneRef));
            }
        }

        private IEnumerator CRLoadSceneAsync(string sceneName)
        {
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            asyncOperation.allowSceneActivation = false;

            while (!asyncOperation.isDone)
            {
                float progress = asyncOperation.progress + 0.1f;

                // activate the new scene when progress is complete 
                if (progress >= 1f)
                {
                    asyncOperation.allowSceneActivation = true;
                    ++numScenesLoaded;
                    yield break; // exit this coroutine
                }

                yield return null; // wait till next frame
            }
        }

        public bool AllScenesFinishedLoading => numScenesLoaded == numScenesToLoad;
    }
}
