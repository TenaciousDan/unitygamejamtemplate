# GameJam Template

- [GameJam Template](#gamejam-template)
  - [Installation](#installation)
  - [Git Configuration](#git-configuration)
    - [Setup Custom Mergetool (optional)](#setup-custom-mergetool-optional)
  - [Getting Started](#getting-started)
    - [Edit Player Settings](#edit-player-settings)


## Installation

In the project explorer view, right click on the `Assets` folder and select `Import Package > Custom Package...`. Then select the `.unitypackage` that you downloaded from this project's `dist` folder.  

If this is a brand new project then:  
  - Export all folders in the unitypackage.  

If this is an existing project then:  
  - Select which folders in the unitypackage you want to import/overwrite.  

Next, Continue to the `Git Configuration` section to configure git or just go straight to `Getting Started` section below.  

## Git Configuration

### Setup Custom Mergetool (optional)

UnityYAMLMerge.exe is a merge tool that aids with the merging of scene, prefab and asset files (and any other YAML files). It comes packaged with each installation of the Unity Editor, therefore if you upgrade the project version you should also update your custom merge tool to use the newer UnityYAMLMerge.exe for your version of Unity.  
  
You must tell Git to use UnityYAMLMerge.exe as a custom merge tool and also as a custom diff tool. It is located in the UnityEditor installation folder under `/Editor/Data/Tools/UnityYAMLMerge.exe`.  
On Windows this is located at `C:/Program Files/Unity/Hub/Editor/{EDITOR_VERSION}/Editor/Data/Tools/UnityYAMLMerge.exe`.  
  
Once you have setup Git to use UnityYAMLMerge, you must then tell UnityYAMLMerge what diff tool to use as a fallback. To do this, you simply need to edit `mergespecfile.txt` located adjacent (within the same folder) to the UnityYAMLMerge.exe file.  
Follow the instructions in this txt file to setup a fallback merge/diff tool.

For more info on UnityYAML merge (Smart Merge) : https://docs.unity3d.com/2020.3/Documentation/Manual/SmartMerge.html

## Getting Started  

### Edit Player Settings

In Unity, navigate to `Edit > Project Settings...`. In the *Project Settings* window, click on `Player` in the left hand menu. Change the `Company Name`, `Product Name`, and any other properties of your choosing.  
